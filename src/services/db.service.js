import db from '../config/lowdb';

export const getPoints = async (endpoint) => {
  const data = db.get(endpoint);
  return data.__wrapped__.points;
};

export const pushPoints = async (endpoint, newElement) => {
  return db.get(endpoint).push(newElement).write();
};

export const deletePoints = async (endpoint) => {
  return db.get(endpoint).remove().write();
};


