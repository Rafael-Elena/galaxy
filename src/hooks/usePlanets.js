import QuestionAngular from '../assets/questions/angular.png';
import QuestionReact from '../assets/questions/react.png';
import QuestionHtml from '../assets/questions/html.png';
import QuestionScss from '../assets/questions/scss.png';
import QuestionCss from '../assets/questions/css.png';
import QuestionJavascript from '../assets/questions/javascript.png';

const angular = {
  planetName: 'Angular',
  planetImage: QuestionAngular,
  planetOptions: {
    error1: '@angular/core',
    error2: '@angular/common',
    correct: 'Rxjs',
  },
};

const react = {
  planetName: 'React',
  planetImage: QuestionReact,
  planetOptions: {
    error1: 'react',
    error2: 'react-route',
    correct: 'react-router-dom',
  },
};

const javascript = {
  planetName: 'Javascript',
  planetImage: QuestionJavascript,
  planetOptions: {
    error1: 'No funciona',
    error2: '$ 95',
    correct: '$ 95 es chollo',
  },
};

const html = {
  planetName: 'Html',
  planetImage: QuestionHtml,
  planetOptions: {
    error1: 'apellido',
    error2: 'surname',
    correct: 'firstSurname',
  },
};

const scss = {
  planetName: 'Scss',
  planetImage: QuestionScss,
  planetOptions: {
    error1: '@include',
    error2: '@mixin',
    correct: '@extend',
  },
};

const css = {
  planetName: 'Css',
  planetImage: QuestionCss,
  planetOptions: {
    error1: ':read-write',
    error2: 'add-text',
    correct: '::after',
  },
};

function usePlanets() {
  return [
    {
      angular,
      react,
      javascript,
      css,
      scss,
      html,
    },
  ];
}

export default usePlanets;