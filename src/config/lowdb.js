import low from 'lowdb';

import localStorage from 'lowdb/adapters/LocalStorage';

const adapter = new localStorage('db');
const db = low(adapter);

db.defaults({
  points: [],
}).write();

export default db;
