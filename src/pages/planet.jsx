import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Div100vh from 'react-div-100vh';

import { getPoints } from '../services/db.service';
import Buttonback from '../components/Buttonback';
import Randomoptions from '../components/Randomoptions';
import Win from '../components/Win';

import CoinOff from '../assets/coins/night.svg';
import CoinOn from '../assets/coins/light.svg';

import '../styles/planet.scss';

function Planet({ planetName, planetImage, planetOptions }) {
  const [isAssertive, setIsAssertive] = useState(false);
  const [hasComplete, setHasComplete] = useState(false);

  useEffect(() => {
    async function getData() {
      let existingPoints = await getPoints('points');
      if (existingPoints.length) {
        let exist = existingPoints.find((point) => {
          return point.id === planetName;
        });
        if (exist) {
          setIsAssertive(true);
        }
        if (existingPoints.length === 6) {
          setHasComplete(true);
        }
      }
    }
    getData();
  }, [isAssertive, planetName]);

  return (
    <Div100vh className="Planet">
      <div className="Planet__buttonstop">
        <span className="Planet__buttonstop-left">
          <Buttonback />
        </span>
        <Link to="/about">
          <button className="Planet__buttonstop-right">Ir a Nave</button>
        </Link>
      </div>
      <h2 className="Planet__title">Planeta {planetName}</h2>
      <div className="Planet__img">
        <img className="Planet__img-question" alt={planetName} src={planetImage} />
        {isAssertive ? (
          <>
            <img alt="coin" className="Planet__img-coin" src={CoinOn} />
            {hasComplete ? (
              <div className="Planet__win">
                <Win />
              </div>
            ) : null}
          </>
        ) : (
          <img alt="coin" className="Planet__img-coin" src={CoinOff} />
        )}
      </div>
      <div className="Planet__buttonbottom">
        {isAssertive ? null : (
          <Randomoptions
            planetName={planetName}
            planetOptions={planetOptions}
            setIsAssertive={setIsAssertive}
          />
        )}
        ;
      </div>
    </Div100vh>
  );
}

export default Planet;
