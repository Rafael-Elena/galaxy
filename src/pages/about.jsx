import Lottie from 'react-lottie';
import Div100vh from 'react-div-100vh';

import Buttonback from '../components/Buttonback';

import LinkUpgrade from '../assets/linkedin/upgradehub.png';
import SpaceshipBottom from '../assets/lotties/spaceship-bottom.json';
import Spaceshiptop from '../assets/spaceships/spaceship-top.png';
import Linkedin from '../assets/linkedin/linkedin.svg';

import '../styles/about.scss';

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: SpaceshipBottom,
};

function About() {
  return (
    <Div100vh className="About">
      <div className="About__button">
        <Buttonback />
        <a href="https://upgrade-hub.com/curso-desarrollador-disenador-frontend/">
          <img className="About__button-upgrade" alt="upgrade" src={LinkUpgrade}></img>
        </a>
      </div>
      <img className="About__top" alt="spaceship" src={Spaceshiptop} />
      <div className="About__spaceship">
        <Lottie options={defaultOptions} />
      </div>
      <div className="About__container">
        <p className="About__container-info">Aplicación realizada por:</p>
        <h2>
          <a
            className="About__container-link"
            href="https://www.linkedin.com/in/victoriamarmili/"
          >
            <img className="About__linkedin" alt="icon" src={Linkedin} />
            María Victoria Marmili
          </a>
        </h2>
        <h2>
          <a
            className="About__container-link"
            href="https://www.linkedin.com/in/rafaelena/"
          >
            <img className="About__linkedin" alt="icon" src={Linkedin} />
            Rafael Elena Torremocha
          </a>
        </h2>
      </div>
    </Div100vh>
  );
}

export default About;
