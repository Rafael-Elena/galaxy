import Div100vh from 'react-div-100vh';

import Planets from '../components/Planets';
import Welcome from '../components/Welcome';
import Score from '../components/Score';

import '../styles/home.scss';

function Home() {
  return (
    <Div100vh className="Home">
      <Welcome />
      <Planets />
      <Score />
    </Div100vh>
  );
}

export default Home;
