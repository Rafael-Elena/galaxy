import { useState, useEffect } from 'react';

import { getPoints, deletePoints } from '../../services/db.service';

import './style.scss';

function Score() {
  const [score, setScore] = useState(0);

  useEffect(() => {
    async function getScore() {
      let existingPoints = await getPoints('points');
      setScore(existingPoints.length);
      if (existingPoints.length === 6) {
        await deletePoints('points');
      }
    }
    getScore();
  }, [score]);

  return (
    <section className="Score">
      <p className="Score__number">{score}/6</p>
      <p className="Score__number">SCORE</p>
    </section>
  );
}

export default Score;
