import Lottie from 'react-lottie';

import WelcomeGalaxy from '../../assets/lotties/galaxygame.json';

import './style.scss';

const defaultOptions = {
  loop: false,
  autoplay: true,
  animationData: WelcomeGalaxy,
};

function Welcome() {
  return (
    <div className="Welcome">
      <Lottie options={defaultOptions}></Lottie>
    </div>
  );
}

export default Welcome;