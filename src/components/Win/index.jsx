import Lottie from 'react-lottie';

import Winner from '../../assets/lotties/thewinner.json';

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: Winner,
};

function Win() {
  return (
    <>
      <Lottie options={defaultOptions} />
    </>
  );
}

export default Win;

