import { useHistory } from 'react-router-dom';

import './style.scss';

function Buttonback() {
  const history = useHistory();

  return (
    <button
      className="Buttonback"
      onClick={() => {
        history.goBack();
      }}
    >
      Ir a Galaxia
    </button>
  );
}

export default Buttonback;
