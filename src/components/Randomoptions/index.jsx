import { useState, useEffect } from 'react';

import { ToastContainer, toast } from 'react-toastify';

import Buttoncoin from '../../components/Buttoncoin/';

import './style.scss';

function Randomoptions({ planetName, planetOptions, setIsAssertive }) {
  const [randomOrder, setRandomOrder] = useState();

  const notify = () =>
    toast.error('👽 ¡Error!', {
      position: 'bottom-center',
      autoClose: 5000,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
    });

  function getRandomOrder() {
    const order = Math.round(Math.random() * 2) + 1;
    setRandomOrder(order);
  }

  useEffect(() => {
    getRandomOrder();
  }, []);

  return (
    <>
      {randomOrder === 1 ? (
        <>
          <Buttoncoin
            setIsAssertive={setIsAssertive}
            option={planetOptions.correct}
            planetName={planetName}
          />
          <button onClick={notify} className="Randomoptions__one">
            {planetOptions.error1}
          </button>
          <button onClick={notify} className="Randomoptions__two">
            {planetOptions.error2}
          </button>
        </>
      ) : null}
      {randomOrder === 2 ? (
        <>
          <button onClick={notify} className="Randomoptions__one">
            {planetOptions.error1}
          </button>
          <Buttoncoin
            setIsAssertive={setIsAssertive}
            option={planetOptions.correct}
            planetName={planetName}
          />
          <button onClick={notify} className="Randomoptions__two">
            {planetOptions.error2}
          </button>
        </>
      ) : null}
      {randomOrder === 3 ? (
        <>
          <button onClick={notify} className="Randomoptions__one">
            {planetOptions.error1}
          </button>
          <button onClick={notify} className="Randomoptions__two">
            {planetOptions.error2}
          </button>
          <Buttoncoin
            setIsAssertive={setIsAssertive}
            option={planetOptions.correct}
            planetName={planetName}
          />
        </>
      ) : null}
      <ToastContainer
        position="bottom-center"
        autoClose={5000}
        closeOnClick
        pauseOnFocusLoss
        pauseOnHover
      />
    </>
  );
}

export default Randomoptions;

