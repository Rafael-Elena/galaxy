import { pushPoints, getPoints } from "../../services/db.service";

import "./style.scss";

const postPoints = async (newCoin) => {
  const existingPoints = await getPoints("points");
  if (existingPoints.length) {
    let exist = existingPoints.find((point) => {
      return point.id === newCoin.id;
    });
    if (!exist) {
      await pushPoints("points", newCoin);
    }
  } else {
    await pushPoints("points", newCoin);
  }
};

function Buttoncoin({ planetName, option, setIsAssertive }) {
  return (
    <button
      className="Buttoncoin"
      onClick={() => {
        postPoints({ id: planetName });
        setIsAssertive(true);
      }}
    >
      {option}
    </button>
  );
}

export default Buttoncoin;
