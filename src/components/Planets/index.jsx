import { Link } from 'react-router-dom';
import { useSpring, animated } from 'react-spring';

import Rocks from '../Rocks';

import PlanetReact from '../../assets/planets/react.png';
import PlanetAngular from '../../assets/planets/angular.png';
import PlanetHtml from '../../assets/planets/planetadonut.svg';
import PlanetJavascript from '../../assets/planets/javascript.png';
import PlanetScss from '../../assets/planets/scss.svg';
import PlanetCss from '../../assets/planets/planetavolcano.svg';
import SpaceShip from '../../assets/spaceships/spaceship.svg';

import './style.scss';

const calc = (x, y) => [x - window.innerWidth / 2, y - window.innerHeight / 2];
const trans1 = (x, y) => `translate3d(${x / -10}px,${y / -10}px,0)`;
const trans2 = (x, y) => `translate3d(${x / 8 + 35}px,${y / 230}px,0)`;
const trans3 = (x, y) => `translate3d(${x / 50}px,${y / 10}px,0)`;
const trans4 = (x, y) => `translate3d(${x / 4.5}px,${y / 4.5}px,0)`;
const trans5 = (x, y) => `translate3d(${x / 50}px,${y / 10}px,0)`;
const trans6 = (x, y) => `translate3d(${x / 5.5}px,${y / 5.5}px,0)`;
const trans7 = (x, y) => `translate3d(${x / -0.6}px,${y / -0.6}px,0)`;
const trans8 = (x, y) => `translate3d(${x / -3}px,${y / -3}px,0)`;

function Planets() {
  const [props, set] = useSpring(() => ({
    xy: [0, 1],
    config: { mass: 60, tension: 550, friction: 140 },
  }));

  return (
    <>
      <Rocks />
      <div
        className="Planets"
        onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}
      >
        <animated.div
          className="Planets__react"
          style={{ transform: props.xy.interpolate(trans1) }}
        >
          <Link to="/react">
            <img className="Planets__react-img" alt="react" src={PlanetReact} />
          </Link>
        </animated.div>
        <animated.div
          className="Planets__angular"
          style={{ transform: props.xy.interpolate(trans2) }}
        >
          <Link to="/angular">
            <img className="Planets__angular-img" alt="angular" src={PlanetAngular} />
          </Link>
        </animated.div>
        <animated.div
          className="Planets__html"
          style={{ transform: props.xy.interpolate(trans3) }}
        >
          <Link to="/html">
            <img className="Planets__html-img" alt="html" src={PlanetHtml} />
          </Link>
        </animated.div>
        <animated.div
          className="Planets__javascript"
          style={{ transform: props.xy.interpolate(trans4) }}
        >
          <Link to="/javascript">
            <img
              className="Planets__javascript-img"
              alt="javascript"
              src={PlanetJavascript}
            />
          </Link>
        </animated.div>
        <animated.div
          className="Planets__scss"
          style={{ transform: props.xy.interpolate(trans5) }}
        >
          <Link to="/scss">
            <img className="Planets__scss-img" alt="scss" src={PlanetScss} />
          </Link>
        </animated.div>
        <animated.div
          className="Planets__css"
          style={{ transform: props.xy.interpolate(trans6) }}
        >
          <Link to="/css">
            <img className="Planets__css-img" alt="css" src={PlanetCss} />
          </Link>
        </animated.div>
        <animated.div
          className="Planets__aliens"
          style={{ transform: props.xy.interpolate(trans7) }}
        />
        <animated.div
          className="Planets__spaceship"
          style={{ transform: props.xy.interpolate(trans8) }}
        >
          <Link to="/about">
            <img alt="about" src={SpaceShip} />
          </Link>
        </animated.div>
      </div>
    </>
  );
}

export default Planets;
