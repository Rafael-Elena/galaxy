import { AnimatedSwitch } from 'react-router-transition';
import { Route } from 'react-router-dom';

import Home from './pages/home';
import About from './pages/about';
import Planet from './pages/planet';

import usePlanets from './hooks/usePlanets.js';

import './App.scss';

function mapStyles(styles) {
  return {
    opacity: styles.opacity,
    transform: `scale(${styles.scale})`,
  };
}

const bounceTransition = {
  atEnter: {
    opacity: 0,
    scale: 1.2,
  },
  atLeave: {
    opacity: 0,
    scale: 4,
  },
  atActive: {
    opacity: 2,
    scale: 1,
  },
};

const numberOfStars = 200;

function App() {
  const [{ angular, react, javascript, css, scss, html }] = usePlanets();

  return (
    <main className="App">
      {[...Array(numberOfStars)].map((e, i) => (
        <span className="App__star" key={i}></span>
      ))}
      <AnimatedSwitch
        atEnter={bounceTransition.atEnter}
        atLeave={bounceTransition.atLeave}
        atActive={bounceTransition.atActive}
        mapStyles={mapStyles}
        className="route-wrapper"
      >
        <Route exact path="/angular">
          <Planet
            planetName={angular.planetName}
            planetImage={angular.planetImage}
            planetOptions={angular.planetOptions}
          />
        </Route>
        <Route exact path="/react">
          <Planet
            planetName={react.planetName}
            planetImage={react.planetImage}
            planetOptions={react.planetOptions}
          />
        </Route>
        <Route exact path="/javascript">
          <Planet
            planetName={javascript.planetName}
            planetImage={javascript.planetImage}
            planetOptions={javascript.planetOptions}
          />
        </Route>
        <Route exact path="/html">
          <Planet
            planetName={html.planetName}
            planetImage={html.planetImage}
            planetOptions={html.planetOptions}
          />
        </Route>
        <Route exact path="/scss">
          <Planet
            planetName={scss.planetName}
            planetImage={scss.planetImage}
            planetOptions={scss.planetOptions}
          />
        </Route>
        <Route exact path="/css">
          <Planet
            planetName={css.planetName}
            planetImage={css.planetImage}
            planetOptions={css.planetOptions}
          />
        </Route>
        <Route exact path="/about">
          <About />
        </Route>
        <Route exact path="/">
          <Home />
        </Route>
      </AnimatedSwitch>
    </main>
  );
}

export default App;
